$(document).ready(function () {
	var swiperWelcome = new Swiper('.swiper-welcome', {
		slidesPerView: 1,
		spaceBetween: 30,
		loop: true,
	})

	var swiperHotProduct = new Swiper('.swiper-hot-product', {
		effect: "slide",
		slidesPerView: 1,
		spaceBetween: 0,
		loop: true,
		navigation: {
			nextEl: '.swiper-hot-product-button-next',
			prevEl: '.swiper-hot-product-button-prev',
		},		
		breakpoints: {
			768: {
			  slidesPerView: 2,
			  spaceBetween: 0,
			},
			1024: {
			  slidesPerView: 5,
			  spaceBetween: 35,
			},
		}
	})
	
    var swiperBrend = new Swiper('.swiper-brend', {
		slidesPerView: 1,
		spaceBetween: 0,
		loop: true,
		navigation: {
			nextEl: '.swiper-brend-button-next',
			prevEl: '.swiper-brend-button-prev',
		},
		pagination: {
			el: '.swiper-brend-pagination',
			clickable: true,
		},
		breakpoints: {
			768: {
			  slidesPerView: 3,
			  spaceBetween: 20,
			},
			1024: {
			  slidesPerView: 6,
			},
		}
	})

	var swiperNews = new Swiper(".swiper-news", {
		slidesPerView: 1,
		spaceBetween: 0,
		loop: true,
		navigation: {
			nextEl: '.swiper-news-button-next',
			prevEl: '.swiper-news-button-prev',
		},
		breakpoints: {
			768: {
			  slidesPerView: 2,
			  spaceBetween: 20,
			},
			1024: {
			  slidesPerView: 3,
			  spaceBetween: 20,
			},
		}
	});

	var swiperProducts = new Swiper(".swiper-products", {
		slidesPerView: 1,
		spaceBetween: 0,
		loop: true,
		navigation: {
			nextEl: '.swiper-products-button-next',
			prevEl: '.swiper-products-button-prev',
		},
		breakpoints: {
			768: {
			  slidesPerView: 2,
			  spaceBetween: 20,
			},
			1024: {
			  slidesPerView: 4,
			  spaceBetween: 20,
			},
		}
	});


	// var galleryThumbs = new Swiper('.gallery-thumbs', {
	// 	lazy: true,
	// 	spaceBetween: 20,
	// 	slidesPerView: 3,
	// 	centeredSlides: true,
	// 	slidesPerView: 'auto',
	// 	touchRatio: 0.2,
	// 	slideToClickedSlide: true,
	// 	loop: true,
	// 	loopedSlides: 3,
		
	//   });
	//   var galleryTop = new Swiper('.gallery-top', {
	// 	spaceBetween: 10,
	// 	lazy: true,
	// 	loop: true,
	// 	loopedSlides: 4,
	// 	navigation: {
	// 	  nextEl: '.swiper-button-next',
	// 	  prevEl: '.swiper-button-prev',
	// 	},
	// 	thumbs: {
	// 	  swiper: galleryThumbs,
	// 	},
	//   });
	  var galleryTop = new Swiper('.gallery-top', {
		spaceBetween: 10,
		navigation: {
		  nextEl: '.swiper-button-next',
		  prevEl: '.swiper-button-prev',
		},
			   loop: true,
			  loopedSlides: 4
	  });
	  var galleryThumbs = new Swiper('.gallery-thumbs', {
		spaceBetween: 10,
		centeredSlides: true,
		slidesPerView: 3,
		touchRatio: 0.2,
		slideToClickedSlide: true,
		loop: true,
		loopedSlides: 4,
		breakpoints: {
			768: {
				slidesPerView: 3,
			},
			1024: {
				slidesPerView: 5,
			},
		}
	  });
	  galleryTop.controller.control = galleryThumbs;
	  galleryThumbs.controller.control = galleryTop;


});
